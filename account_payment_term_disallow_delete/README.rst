.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=============================================================
Account Payment Term: Disallow deleting terms that are in use
=============================================================

Don't allow deleting payment terms, if they are in use for:

 * Partners
 * Sale orders
 * Purchase orders
 * Account moves

Configuration
=============
* None needed

Usage
=====
* Try to delete a payment term that is in use, and an error message is shown.

Known issues / Roadmap
======================
\-

Credits
=======

Contributors
------------

* Jarmo Kortetjärvi <jarmo.kortetjarvi@tawasta.fi>
* Miika Nissi <miika.nissi@tawasta.fi>
* Timo Talvitie <timo.talvitie@tawasta.fi>

Maintainer
----------

.. image:: https://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: https://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
