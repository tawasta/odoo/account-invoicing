.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=====================================================================
Account Invoice Commission - Create commission payments from invoices
=====================================================================

* Allows Making commission payments from invoices

Configuration
=============
* Optionally set the defaut commission communication text from Invoicing Settings

Usage
=====
* Go to invoice tree view, select paid customer invoices, and launch the 
  commission wizard from Actions -> Create commission payments
* Afterward you can print a commission report from the created
  vendor payment via Print -> Payment Commissions

Known issues / Roadmap
======================
\-

Credits
=======

Contributors
------------
* Jarmo Kortetjärvi <jarmo.kortetjarvi@tawasta.fi>
* Timo Talvitie <timo.talvitie@tawasta.fi>

Maintainer
----------

.. image:: https://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: https://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
